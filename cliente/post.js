function crear_form(nombre,id){
  var obj=document.createElement("form");
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_input_text(nombre,id)
{
  var obj=document.createElement("input");
  obj.setAttribute("type","text");
  obj.setAttribute("name",nombre);
  obj.setAttribute("placeholder",nombre);
  obj.setAttribute("id",id);
  return obj;
}

function crear_button(nombre,id,cadena)
{
  var obj=document.createElement("button");
  obj_text=document.createTextNode(cadena);
  obj.appendChild(obj_text);
  obj.setAttribute("name",nombre);
  obj.setAttribute("id",id);
  obj.setAttribute("type","button");
  return obj;
}

function div(cadena,objeto)
{
  var obj=document.createElement("div");
  obj.setAttribute("class","row");
  var ob=document.createElement("div");
  ob.setAttribute("class","col-md-2");
  var titulo=document.createElement("h1");
  var ob_text=document.createTextNode(cadena);
  ob.appendChild(ob_text);
  var ob1=document.createElement("div");
  ob1.setAttribute("class","col-md-10");
  ob1.appendChild(objeto);
  obj.appendChild(ob);
  obj.appendChild(ob1);
  return obj;
}


function crear_container(cadena)
{
  var obj=document.createElement("div");
  obj.setAttribute("class","container-fluid");
  obj.setAttribute("id",cadena);
  var ob=document.createElement("legend");
  var ob_text=document.createTextNode(cadena);
  ob.appendChild(ob_text);
  obj.appendChild(ob);
  return obj;
}


function asociar_hijo(hijo,padre)
{
  padre.appendChild(hijo);
}


function crear_select(nombre,id,opcion,cambio)
{
  var obj=document.createElement("select");
  obj.setAttribute("type","select");
  obj.setAttribute("name",nombre);
  obj.setAttribute("onChange",cambio);
  obj.setAttribute("id",id);
  for(var i=0;i<opcion.length;i++)
  {
    var op=document.createElement("option");
    op_text=document.createTextNode(opcion[i][1]);
    op.appendChild(op_text);
    op.setAttribute("value",opcion[i][0]);
    obj.appendChild(op);
  }
  return obj;
}



function iniciar()
{
  var url="http://js.wapp.bitnami/poblacionyvivienda/temas/";
    var objeto_xhr=new XMLHttpRequest();
  objeto_xhr.onreadystatechange =function(){
  if(this.readyState===4&&this.status===200)
  {
    console.log("exito");
    console.log(objeto_xhr.responseXML); 
    var objeto=objeto_xhr.responseXML.firstChild.children;

titulo=[];

for(var i=0;i<objeto.length;i++)
{
  var id=objeto[i].children[0].textContent;
  var des=objeto[i].children[1].textContent;
  titulo[i]=[id,des];
}
  
  var cuerpo=document.body;
  var contenedor=crear_container("CENSO POBLACION Y VIVIENDA 2010");
  var formulario=crear_form("formulario","101");
  var selec=crear_select("opciones","102",titulo,"valor()");
    var elemento=div("AGREGAR NUEVO TEMA EN: ",selec);

  asociar_hijo(formulario,cuerpo);
  asociar_hijo(contenedor,formulario);
  asociar_hijo(elemento,contenedor);
  }}
  objeto_xhr.open('GET',url);
  objeto_xhr.setRequestHeader('accept','application/xml');  
  objeto_xhr.send(); 
  
}



function valor()
{
  var contenedor=document.getElementById("CENSO POBLACION Y VIVIENDA 2010");
  var texto1=crear_input_text("texto","1111");
var boton=crear_button("boton1","1113","GUARDAR");
var estilo1=div("Descripcion: ",texto1);
var estilo3=div("",boton);
asociar_hijo(estilo1,contenedor);
asociar_hijo(estilo3,contenedor);
  
      boton=document.getElementById("1113");
boton.addEventListener('click',post,false);
}


function post()
{
  var op=document.formulario.opciones.value;
  var des=document.formulario.texto.value;
  var url="http://js.wapp.bitnami/poblacionyvivienda/temas/"+op+"/subtemas";
var objeto_xhr=new XMLHttpRequest();
  objeto_xhr.onreadystatechange =function(){
  if(this.readyState===4&&this.status===201)
  {
    console.log("exito");
    console.log(objeto_xhr.responseXML.firstChild.children[0].textContent);
  }}
  objeto_xhr.open('POST',url);
  objeto_xhr.setRequestHeader('accept','application/xml');
  var cuerpo="<tema2><descripcion>"+des+"</descripcion></tema2>";
//  objeto_xhr.sendAsBinary(cuerpo); 
  objeto_xhr.send(cuerpo); 
}












iniciar();
