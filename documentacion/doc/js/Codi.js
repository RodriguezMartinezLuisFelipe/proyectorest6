//funcioes div
function crear_div(atributos){
    obj_div = document.createElement("div");
    for(var i=0; i< atributos.length;i++){
      obj_div.setAttribute(atributos[i]["atributo"],atributos[i]["valor"]); 
     }
        return obj_div;
    }
    //crear br
    function crea_br(){
    obj_br = document.createElement("br");
        return obj_br;
    }
    //crear p
    function crea_p(){
    obj_p = document.createElement("p");
    return obj_p;
    }
    //crear textNode
    function crea_t(texs){
    obj_br = document.createTextNode(texs);
    return obj_br;
    }
    //formularios
function crear_form(atributos){
    obj_form = document.createElement("form");
    for(var i=0; i< atributos.length;i++){
      obj_form.setAttribute(atributos[i]["atributo"],atributos[i]["valor"]); 
     }
        return obj_form;
    }
//input
function crear_input(atributos){
   obj_input = document.createElement("input");
    for(var i=0; i< atributos.length;i++){
      obj_input.setAttribute(atributos[i]["atributo"],atributos[i]["valor"]); 
     }
        return obj_input;
    }
//selecte
function crear_select(atributos,opciones){
    obj_select = document.createElement("select");
    obj_optgroup = document.createElement("optgroup");
    for(var i=0; i< atributos.length;i++){
      obj_optgroup.setAttribute(atributos[i]["atributo"],atributos[i]["valor"]); 
     }
     for(var j=0; j < opciones.length; j++){
       obj_opcion=document.createElement("option");
       obj_opcion.textContent= opciones[j];
       obj_optgroup.appendChild(obj_opcion);
     }
        obj_select.appendChild(obj_optgroup);
        return obj_select;
}
//crear TextArea
function crear_ta(atributos){
   obj_ta = document.createElement("textarea");
    for(var i=0; i< atributos.length;i++){
      obj_ta.setAttribute(atributos[i]["atributo"],atributos[i]["valor"]); 
     }
        return obj_ta;
    }
//asocia los hijos    
function asociar_hijo(obj,padre){
        padre.appendChild(obj);
    }
/*****************************************************/ 
    div1=[];
	div1[0]={"atributo":"class","valor":"navbar navbar-inverse navbar-fixed-top"};
    crear_div(div1);
    asociar_hijo(obj_div,document.body);
/*****************************************************************************************/    
    div2=[];
	div2[0]={"atributo":"class","valor":"container"};
    obj0=crear_div(div2);
    asociar_hijo(obj0,document.body);	
    div3=[];
	div3[0]={"atributo":"class","valor":"jumbotron"};
    abj1=crear_div(div3);
    asociar_hijo(abj1,obj0);
    div4=[];
	div4[0]={"atributo":"class","valor":"container"};
    obj1=crear_div(div4);
    asociar_hijo(obj1,abj1);
    div5=[];
	div5[0]={"atributo":"class","valor":"row"};
    obj3=crear_div(div5);
    asociar_hijo(obj3,obj1);
    div6=[];
	div6[0]={"atributo":"class","valor":"col-md-4"};
    obj5=crear_div(div6);
    asociar_hijo(obj5,obj3);
    div7=[];
	div7[0]={"atributo":"class","valor":"col-md-4"};
    obj6=crear_div(div7);
    asociar_hijo(obj6,obj3);
    div8=[];
	div8[0]={"atributo":"class","valor":"row"};
    obj7=crear_div(div8);
    asociar_hijo(obj7,obj6);
    div9=[];
	div9[0]={"atributo":"class","valor":"col-md-4"};
    obj8=crear_div(div9);
    asociar_hijo(obj8,obj7);
    div10=[];
	div10[0]={"atributo":"class","valor":"col-md-4"};
    obj9=crear_div(div10);
    asociar_hijo(obj9,obj7);
  
/****************************************************************************/    
form=[];
form[0]={"atributo":"nombre","valor":"formulario"};
input1=[];
input1[0]={"atributo":"type","valor":"text"};
//para el select
select=[];
select[0]={"atributo":"label","valor":"sexo"}
opciones=["masculino","femenino"];
//textArea
ta=[];
ta[0]={"atributo":"cols","valor":"25"};
ta[1]={"atributo":"rows","valor":"3"};
ta[2]={"atributo":"name","valor":"algo"};

 
//Nombre
crear_form(form);
br1=crea_br();
asociar_hijo(br1,obj_form);
z=crea_p();
y=crea_t("nombre");
asociar_hijo(y,z);
br2=crea_br();
asociar_hijo(br2,obj8);
asociar_hijo(z,obj8);
crear_input(input1);
asociar_hijo(obj_input,obj_form);
asociar_hijo(obj_form,obj9);

//Edad
form=crear_form(form);
br1=crea_br();
asociar_hijo(br1,form);
z=crea_p();
y=crea_t("edad");
asociar_hijo(y,z);
asociar_hijo(z,obj8);
crear_input(input1);
asociar_hijo(obj_input,form);
asociar_hijo(form,obj9);

//Sexo
form1=crear_form(form);
br1=crea_br();
asociar_hijo(br1,form1);
z=crea_p();
y=crea_t("sexo");
asociar_hijo(y,z);
asociar_hijo(z,obj8);
crear_select(select,opciones);
asociar_hijo(obj_select,form1);
asociar_hijo(form1,obj9);

//observaciones
form2=crear_form(form);
br1=crea_br();
asociar_hijo(br1,form2);
z=crea_p();
y=crea_t("coments");
asociar_hijo(y,z);
asociar_hijo(z,obj8);
tas=crear_ta(ta);
asociar_hijo(tas,form2);
asociar_hijo(form2,obj9);

