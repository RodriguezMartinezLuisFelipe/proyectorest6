<?php 
require '../../app/vendor/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->config(array(
    'templates.path' => '../../app/templates'
));

define("SPECIALCONSTANT", true);
require '../../app/routes/api.php';
require '../../app/routes/connect.php';
$app->run();



       
