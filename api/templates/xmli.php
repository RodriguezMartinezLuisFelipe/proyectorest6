<?xml version="1.0" encoding="UTF-8"?>
<censo>
<?php foreach ($resultados as $b) { ?>
<temanivel1 id="<?php echo $b['id_t1']; ?>">
	<temanivel2 id="<?php echo $b['id_t2']; ?>">
		<temanivel3 id="<?php echo $b['id_t3']; ?>">
			<indicador>
				<id><?php echo $b['id_indicador']; ?></id>
				<descripcion><?php echo $b['descripcion']; ?></descripcion>
				<conteo><?php echo $b['anio']; ?></conteo>
			</indicador>
		</temanivel3>
	</temanivel2>
</temanivel1>
<?php } ?>
</censo>